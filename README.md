## lancelot-user 11 RP1A.200720.011 V12.0.1.0.RJCMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: lava
- Brand: Redmi
- Flavor: aosp_lava-userdebug
- Release Version: 12
- Id: SQ1D.220105.007
- Incremental: 1642607769
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/lancelot_global/lancelot:11/RP1A.200720.011/V12.0.1.0.RJCMIXM:user/release-keys
- OTA version: 
- Branch: lancelot-user-11-RP1A.200720.011-V12.0.1.0.RJCMIXM-release-keys
- Repo: redmi_lava_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
